package upv.practica08;

import android.app.Activity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;


/**
 * Created by Fernando on 13/06/2015.
 */
public class SMSActivity extends Activity{

    Button btnEnviar;
    TextView txtTelefono;
    String telefono, lon, lat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sms_activity);

        btnEnviar = (Button)findViewById(R.id.btnEnviar);
        txtTelefono = (TextView)findViewById(R.id.txtTelefono);

        telefono = txtTelefono.getText().toString();

        btnEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Bundle extras = getIntent().getExtras();
                if (extras != null) {
                    lon = extras.getString("longitud");
                    lat = extras.getString("latitud");
                }

                SmsManager smsManager = SmsManager.getDefault();
                smsManager.sendTextMessage(telefono, null, "Longitud: " + lon + "\n" +
                        "Latitud: " + lat, null, null);

                String path = getBaseContext().getFilesDir().getAbsolutePath();
                File file = new File(path + "/file.txt");

                if(!file.exists())
                {
                    try {
                        file.createNewFile();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                try {
                    FileOutputStream stream = null;
                    stream = new FileOutputStream(file);
                    String data = "Numero: " + telefono + "\n" +
                            "Longitud: " + lon + "\n" +
                            "Latitud: " + lat;
                    stream.write(data.getBytes());
                    stream.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Toast.makeText(getApplicationContext(),
                        "Mensaje Enviado", Toast.LENGTH_LONG).show();
            }
        });

    }
}
